// always do this first:
'use strict';

// declare a new subclass of turck.App, called DefaultApp.
turck.DefaultApp = class extends turck.App {
  // create a constructor for your class. We will call this constructor at the
  // end of the script to create your instance of the app.
  constructor() {
    super(); // don't forget to call super!
    // the names of these variables are pretty self-explanatory. When assigning
    // colors, you can either use a hexadecimal code (e.g. "#FF0000" is red), or
    // one of the built-in colors like those being used below. A full list of
    // built-in colors is shown in section 8.5.1
    this.themeColor = "@color/turck_yellow";
    this.textColorOnThemeColor = "@color/black";
    this.buttonColor = "@color/turck_yellow_darker";
    this.buttonPressedColor = "@color/black";
    this.textColorOnBackground = "@color/black";
    this.titleViewUseBackgroundBasedOnThemeColor = true;
  }

  // onLaunch gets called when the app first starts up. This function should
  // define the things to be shown on the gadget page ("gadget page" is the
  // in-code name for the app's home screen).
  onLaunch(launchAction, launchPath, launchParameters) {

    // showPage() tells the app to show a page. In this case, we define an
    // instance of the GadgetPage class. Other Page class instances are shown
    // further down in the code.
    app.showPage(new turck.ui.GadgetPage({
      // Show dotted background and curves
      backgroundImage: "@drawable/bg_dots",
      backgroundImageMode: "tile",

      // Some more color variables with self-explanatory names
      underStatusBarColor: "@color/turck_yellow",
      gradientTopColor: "@color/home_gradient_top",
      gradientBottomColor: "@color/home_gradient_bottom",
      curveColor: "@color/home_circles_stroke",
      belowBottomCircleColor: "@color/bottom_circle_fill",

      // Since this is a gadget page, we must provide a list of "gadgets."
      gadgets:[
        // The first gadget is a turck.ui.Image. This is the Turck logo that
        // appears at the top of the app when it first starts up.
        new turck.ui.Image({
          position: [{type: "parent", ratio: 0.5, at: "center"},
                     {type: "parent", offset: 0, at: "top"}],
          image: "@drawable/logo_turck",
          backgroundColor: "@color/turck_yellow",
          padding: [-1, 10, -1, 10]
        }),
        // The next gadget is the BL Ident logo, which shows up at the top,
        // offset just a little bit below the Turck logo defined above.
        new turck.ui.Image({
          position: [{type: "parent", ratio: 0.5, at: "center"},
                     {type: "parent", offset: 65, at: "top"}],
          image: "@drawable/logo_blident"
        }),
        // This gadget is a StatusImage. This doesn't do anything for a PD67,
        // but on a phone connected to a PD20, it will show the status of the
        // connection.
        new turck.ui.StatusImage({
          position: [{type: "parent", ratio: 1.0, offset: -10, at: "right"},
                     {type: "parent", offset: 48, at: "top"}],
          displayVersionInfoOnTouch: true
        }),
        // This gadget is a TextAndImageButton. The variables that define
        // its appearance should be pretty straightforward.
        new turck.ui.TextAndImageButton({
          position: "curve",
          text: "@string/HomePageScanButton",
          buttonColor: "@color/home_button_text",
          buttonPressedColor: "@color/turck_yellow",
          image: "@drawable/scan_button",
          // when the user taps the button, this function will run. The function
          // defined here will call the function called showScanPage.
          click: function() { app.showScanPage(false); }
        }),
        // Another button like the one above. This one is for the read/write
        // page of the app.
        new turck.ui.TextAndImageButton({
          position: "curve",
          text: "@string/HomePageReadWriteButton",
          buttonColor: "@color/home_button_text",
          buttonPressedColor: "@color/turck_yellow",
          image: "@drawable/read_write_button",
          // When this button is pressed, the below function is called. the
          // function calls another function: showReadWritePage.
          click: function() { app.showReadWritePage(); }
        }),
        // Settings button (long press displays additional setings)
        new turck.ui.TextAndImageButton({
          position: "bottom.center",
          text: "@string/HomePageSettingsButton",
          buttonColor: "@color/home_button_text",
          buttonPressedColor: "@color/turck_yellow",
          image: "@drawable/settings_button",
          // the click function works the same as the other two buttons above.
          // in this case, when the button is clicked, the showSettingsPage
          // function is called.
          click: function() { app.showSettingsPage(false); },
          // if you define a longClick function for a button gadget, it will be
          // called if the user presses and holds the button for a couple
          // seconds. the two buttons above could also have longClick functions,
          // if you wanted them.
          longClick: function() { app.showSettingsPage(true); }
        }),
        // Yet another button gadget. This one is for the barcode scanner page.
        new turck.ui.TextAndImageButton({
          // the visibility parameter allows a gadget to only be shown under
          // a certain condition. In this case, the button is only shown if
          // the PDxx has a barcode reader. Using 'visibility: "hasHf"' will
          // make a gadget appear only if the PDxx has an HF reader. You can
          // also pass a JS boolean into the visibility parameter.
          visibility: "hasBarcode",
          position: [{type: "parent", ratio: 0.8, at: "center"},
                     {type: "parent", ratio: 0.65, at: "center"}],
          text: "@string/HomePageBarcodeButton",
          buttonColor: "@color/home_button_text",
          buttonPressedColor: "@color/turck_yellow",
          image: "@drawable/barcode_button",
          click: function() { app.showBarcodePage(); }
        }),
        // The UhfHfSegmentedControl gadget allows the user to switch the PDxx
        // between UHF and HF modes. You can use that 'visibility' parameter
        // on any gadget except this one- the UhfHfSegmentedControl gadget
        // ignores the 'visibility' parameter, and will automatically hide
        // itself if the PDxx doesn't have HF capabilities.
        new turck.ui.UhfHfSegmentedControl({
          position: [{type: "parent", ratio: 0.25, at: "center"},
                     {type: "parent", ratio: 0.32, at: "center"}]
        })
      ]}));
    // always put this at the end of the onLaunch function!
    app.handleLaunchParameters(launchAction, launchPath, launchParameters);
  }

  // showSettingsPage was called by some of the 'click' functions on the gadget
  // page. There is not much to customize with the settings page. It is set up
  // for you.
  showSettingsPage(showAdvancedSettings) {
    app.showPage(new turck.ui.SettingsPage({
      titleText: "@string/HomePageSettingsButton",
      showAdvancedSettings: showAdvancedSettings
    }));
  }

  // showScanPage shows the scan page.
  showScanPage(startScanning, launchAction, launchPath, launchParameters) {
    app.showPage(new turck.ui.ScanPage({
      startScanning: startScanning,
      launchAction: launchAction,
      launchPath: launchPath,
      launchParameters: launchParameters,
      titleText: "@string/HomePageScanButton",

      // these define what each footer button DOES, not the text they display.
      // here, we set the left footer button to pause and resume RFID scans,
      // the center footer button stops and starts RFID scans, and the right
      // footer button opens the scan configuration menu. You can set any
      // of the 3 footer buttons to any of these 3 options.
      footerLeft: "pauseResume",
      footerCenter: "startStop",
      footerRight: "configure",

      // this function is expected to return the scan configuration, which is
      // set up by the scan configuration menu. Don't touch this.
      getScanConfiguration: function() {
        return turck.settings.scanConfiguration;
      },

      // this function is expected to return the destination for scanned data.
      // destination is configured in the scan configuration menu.
      // Don't touch this.
      getDestination: function() {
        return turck.settings.destination;
      },

      // the tagFound() function gets called when a tag is found during a scan.
      // if the function returns true, the tag will be added to the list of
      // found tags on the screen. If the function returns false, the tag
      // will not be added to that list.
      tagFound: function(tag) {
        // calling this will cause the tag data to be sent to the destination
        // defined in the scan configuration menu.
        this.sendTag(tag);
        return true;
      },

      // afterScan gets called when a scan has finished.
      afterScan: function(tags, displayResultToUser, completion) {
        // sendTags sends all the tags as a group.
        this.sendTags(tags, displayResultToUser, completion);
      },

      // tagTouched gets called when the user taps a tag in the scan page.
      tagTouched: function(tag) {
        if (!turck.settings.destination) {
          // shows the read-write page, loading it with this tag's epc. Then,
          // finishes this page.
          app.showReadWritePage(tag.epc);
          this.finish();
        }
      },

      // this function is called when the PD67's left programmable button is
      // pressed. Change this function to change the left programmable button's
      // behavior. Any page (this scan page, the read/write page, the gadget
      // page from before, etc) can have a function like this.
      onPD67LeftButtonPress() {
        this.toggleScan(); // toggle scanning
      },

      // this function is called when the PD67's right programmable button is
      // pressed. Change this function to change the right programmable button's
      // behavior. Any page (this scan page, the read/write page, the gadget
      // page from before, etc) can have a function like this.
      onPD67RightButtonPress() {
        if (this.isPaused) {
          // if the scan is paused, show a "resuming scan" message, and resume
          // the scan.
          app.showTimedToast(
            app.getLocalizedAppString("@string/ScanPageFooterResume"), "", 0.8);
          this.resumeScan();
        } else if (this.isScanning) {
          // otherwise, if we are currently scanning, show a "pausing scan"
          // message, and pause the scan.
          app.showTimedToast(
            app.getLocalizedAppString("@string/ScanPageFooterPause"), "", 0.8);
          this.pauseScan();
        }
      },

      // if the app is running on a PD67, and that PD67 has the trigger
      // accessory, this function will get called when the trigger is pulled.
      // Any page (this scan page, the read/write page, the gadget page from
      // before, etc) can have a function like this.
      onPD67TriggerButtonPress() {
        this.toggleScan();
      }
    }));
  }

  // the showReadWritePage function shows the read write page. If an epc is
  // passed into the function, it will load that tag automatically.
  showReadWritePage(epc) {
    app.showPage(new turck.ui.ReadWritePage({
      epc: epc,
      titleText: "@string/HomePageReadWriteButton",

      // runs when a tag is found. if this is a tag we want to read/write,
      // return true. if we want to ignore this tag, return false.
      tagFound: function(tag) {
        return true;
      },

      // onPD67RightButtonPress works the same way it did above.
      onPD67RightButtonPress: function() {
        if (!this.isScanning) {
          this.writeTag();
        }
      },

      // onPD67LeftButtonPress works the same way it did above.
      onPD67LeftButtonPress: function() {
        if (!this.isScanning) {
          this.readTag();
        }
      },

      // onPD67TriggerButtonPress works the same way it did above.
      onPD67TriggerButtonPress: function() {
        if (!this.isScanning) {
          this.readTag();
        }
      }

    }));
  }

  // showBarcodePage shows the barcode page.
  showBarcodePage() {
    app.showPage(new turck.ui.BarcodePage({
      titleText: "@string/HomePageBarcodeButton",

      // these define what each footer button DOES, not the text they display.
      // setting one to "scan" makes that footer button start or stop a barcode
      // scan. setting it to "copy" makes that button copy the scanned barcode
      // to the Android clipboard.
      footerCenter: "scan",
      footerRight: "copy",

      // this function gets called when a barcode is found.
      barcodeScanned: function(barcode) {
        // displayBarcode() makes the barcode value appear on the screen.
        this.displayBarcode(barcode);
      },

      // onPD67RightButtonPress works the same way it did above.
      onPD67TriggerButtonPress: function() {
        if (!this.isScanning) {
          this.startBarcodeScan();
        } else {
          this.cancelBarcodeScan();
        }
      },

      // onPD67LeftButtonPress works the same way it did above.
      onPD67LeftButtonPress: function() {
        if (!this.isScanning) {
          this.startBarcodeScan();
        } else {
          this.cancelBarcodeScan();
        }
      },

      // onPD67RightButtonPress works the same way it did above.
      onPD67RightButtonPress: function() {
        if (!this.isScanning) {
          this.startBarcodeScan();
        } else {
          this.cancelBarcodeScan();
        }
      }
    }));
  }

}

// Create the app object. very important!
var app = new turck.DefaultApp();
