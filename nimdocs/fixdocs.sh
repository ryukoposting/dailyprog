#!/bin/bash

if [ "$1" = "" ]; then
  find | grep ".html$" | sed 's,^,./fixdocs.sh ,g' | sh
else
  echo "$1"
  cp "$1" "$1.old"
  awk -f fix_docs.awk "$1.old" > "$1"
fi
