#!/bin/bash

if [ "$1" = "" ]; then
  ls -d */ | sed 's,^,./waifugen.sh ,g' | sh
else
  name=`echo "$1" | tr -dc '[:alnum:]'`
  echo $name
  items=`ls $name | grep -vE '^_.*' | grep -E '(png$)|(jpg$)|(jpeg$)|(gif$)' | sed "s,^,\"./$name/,g" | sed 's/$/", /g'`
  items=${items::-2}
  
  cat header.txt > $name.html
  printf "   <title>$name</title>" >> $name.html
  cat middle.txt >> $name.html
  echo "        var imgAr = [$items]" >> $name.html
  cat footer.txt >> $name.html
fi
